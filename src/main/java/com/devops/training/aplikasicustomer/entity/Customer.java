package com.devops.training.aplikasicustomer.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

@Entity @Data //dari Lombok untuk getter Setter
public class Customer {

	@Id @GeneratedValue(generator = "uuid")//Generator Tidak akan sama
	@GenericGenerator(name="uuid", strategy = "uuid2")
	private String id;
	
	@NotEmpty @Size(min=3, max=255)
	private String fullname;
	
	@Email
	@NotEmpty @Size(min=6, max=100)
	private String email;
	
	@NotEmpty @Size(min=5, max=20)
	private String mobile_phone;
}
