package com.devops.training.aplikasicustomer.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.devops.training.aplikasicustomer.entity.Customer;

public interface CustomerDao extends PagingAndSortingRepository<Customer, String> {

}
