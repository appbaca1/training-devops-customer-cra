create table customer (
    id varchar(36),
    fullname varchar(255) not null,
    email varchar(100) not null,
    mobile_phone varchar(20) not null,
    primary key (id),
    unique(email),
    unique(mobile_phone)
);
